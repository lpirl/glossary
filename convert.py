#!/usr/bin/env python3

'''
Converts glossary from spreadsheet to tex.
'''

# scripting is not very elegant and a bit dense but it does the job

import argparse
from abc import ABC, abstractmethod
from collections import OrderedDict
from csv import DictReader
from glob import glob
from os.path import splitext
from re import compile
from subprocess import check_call
from tempfile import TemporaryDirectory

ID_COLUMN_NAME = 'id'

CONVERTERS = []


class AbstractConverter(ABC):

  REMOVE_LATEX_REGEX = compile(r'\\\w+({([^}]*)})?')

  def remove_latex(self, what):
    n = -1
    while n != 0:
      what, n = self.REMOVE_LATEX_REGEX.subn(r'\2', what)
    return what

  @abstractmethod
  def get_ext(self):
    pass

  def get_preamble(self):
    pass

  def get_postamble(self):
    pass

  def get_converted_row(self, idx, row):
    pass


@CONVERTERS.append
class TexConverter(AbstractConverter):

  # see https://ftp.fau.de/ctan/macros/latex/contrib/glossaries/glossaries-user.html#sec:gloskeys
  PREDEFINED_KEYS = {
    'access',
    'counter',
    'description',
    'descriptionaccess',
    'descriptionplural',
    'descriptionpluralaccess',
    'first',
    'firstaccess',
    'firstplural',
    'firstpluralaccess',
    'long',
    'longaccess',
    'longplural',
    'longpluralaccess',
    'name',
    'nonumberlist',
    'parent',
    'plural',
    'pluralaccess',
    'see',
    'short',
    'shortaccess',
    'shortplural',
    'shortpluralaccess',
    'sort',
    'symbol',
    'symbolaccess',
    'symbolplural',
    'symbolpluralaccess',
    'text',
    'textaccess',
    'type',
    'user1',
    'user2',
    'user3',
    'user4',
    'user5',
    'user6',
  }

  ADDKEY_COMMANDS = (
    'glsentry',
    'Glsentry',
    'gls',
    'Gls',
    'GLS'
  )

  REPLACEMENTS = (
    (': ', ':\\ '),
    (' \\cite', '~\\cite'),
  )

  def __init__(self):
    super().__init__()
    self.keys_seen = set()


  def get_ext(self):
    return 'tex'


  def _get_custom_key_preamble(self, key):

    lines = []
    lines.append(f'\\glsaddkey*{{{key}}}%')
    lines.append(f'  {{}}%')
    for command in self.ADDKEY_COMMANDS:
      lines.append(f'  {{\\internal{command}{key}}}%')

    for command in self.ADDKEY_COMMANDS:
      lines.append(f'\\newcommand*{{\\{command}{key}}}[1]{{%')

      if command.endswith('entry'):
        # command is insensitive to first use flag
        lines.append(f'  \\internal{command}{key}{{#1}}%')
      else:
        # command is sensitive to first use flag
        lines.append(f'  \\ifglsused{{#1}}{{%')
        lines.append(f'    \\internal{command}{key}{{#1}}%')
        lines.append(f'  }}{{%')
        lines.append(f'    \\ifglshasfield{{first{key}}}{{#1}}{{%')
        lines.append(f'      \\internal{command}first{key}{{#1}}%')
        lines.append(f'    }}{{%')
        lines.append(f'      \\internal{command}{key}{{#1}}%')
        lines.append(f'    }}%')
        lines.append(f'    \\glsunset{{#1}}%')
        lines.append(f'  }}%')

      lines.append(f'}}')

    return '\n'.join(lines)


  def get_preamble(self):
    lines = [
      '\\usepackage{glossaries}',
      '\\newignoredglossary{hidden}',
      '',
    ]
    lines.extend(
      self._get_custom_key_preamble(key)
      for key in self.keys_seen - self.PREDEFINED_KEYS
    )
    lines.append('') # make preamble end with a newline
    lines.append('') # even more, insert blank line after preamble
    return '\n'.join(lines)


  def _prefix_description(self, row):
    if not 'long' in row:
      return
    first_closing_brace = row['long'].find('}')
    if first_closing_brace < 0:
      last_opening_brace = 0
    else:
      last_opening_brace = \
        row['long'].rfind('{', 0, first_closing_brace) + 1

    row['description'] = (
      row['long'][:last_opening_brace] +
      '\\MakeUppercase{' +
      row['long'][last_opening_brace:last_opening_brace+1] +
      '}' +
      row['long'][last_opening_brace+1:] +
      '. ' +
      row['description']
    )


  def get_converted_row(self, idx, row):

    # happens anyway, but explicit is better than implicit:
    row = row.copy()

    # preprocess all values
    for old, new in self.REPLACEMENTS:
      row = {k: v.replace(old, new) for k, v in row.items()}

    # for each field "computed" here, prefer ``setdefault`` and order
    # code hence in priority from high to low

    assert 'short' in row or 'long' in row, \
      f'need field "short" or "long" in row: {row}'
    if 'short' in row:
      row.setdefault('name', row['short'])
    else:
      row.setdefault('name', row.pop('long'))

    row.setdefault('description', '')

    # plural
    assert 'plural' not in row, \
      f'use "shortplural" instead of "plural" in source data'
    if 'shortplural' in row:
      row['plural'] = row.pop('shortplural')
    elif 'longplural' in row:
      row.setdefault('plural', row["longplural"])

    # first
    if 'short' in row and 'long' in row:
      row.setdefault('first', f'{row["long"]} ({row["short"]})')

    # firstplural
    if 'longplural' in row:
      if 'shortplural' in row:
        row.setdefault('firstplural',
                       f'{row["longplural"]} ({row["shortplural"]})')
      elif 'short' in row:
        row.setdefault('firstplural',
                       f'{row["longplural"]} ({row["short"]}s)')
      else:
        row.setdefault('firstplural', row["longplural"])

    # description
    if 'short' in row:
      self._prefix_description(row)
    row['description'] = row['description'].strip().strip('.')
    if not row['description']:
      row.setdefault('type', 'hidden')

    self.keys_seen.update(row.keys())

    # finally, assemble the glossary entry from the modified row
    lines = [f'\\newglossaryentry{{{idx}}}{{%']
    for key, value in row.items():
      lines.append(f'  {key}={{{value}}},')
    lines.append('}')
    lines.append('')

    return '\n'.join(lines)


@CONVERTERS.append
class RstConverter(AbstractConverter):

  def get_ext(self):
    return 'rst'

  def get_converted_row(self, idx, row):
    def field(key):
      value = row[key]
      value = value.replace('\n', '\n  ')
      value = self.remove_latex(value)
      return value

    lines = []

    if 'short' in row:
      lines.append(f'.. |{idx}| replace:: {field("short")}')
    else:
      lines.append(f'.. |{idx}| replace:: {field("long")}')

    for key in row:
      value = field(key)
      lines.append(f'.. |{idx}-{key}| replace:: {value}')

    if 'description' in row:
      lines.append(f'.. _gl-{idx}:')
      lines.append('')
      lines.append(f'  |{idx}-long|')
      if 'short' in row:
        lines.append('')
        lines[-2] += f' (|{idx}-short|)'
      lines.append(f'  |{idx}-description|')

    lines.append('') # end with a line break
    lines.append('') # add even add a blank line

    return '\n'.join(lines)


@CONVERTERS.append
class LanguagetoolSpellingTxtConverter(AbstractConverter):

  WHITESPACE_REGEX = compile(r'\s+')
  SPLIT_NONWORD_REGEX = compile(r'[^\w-]+')

  def remove_newlines(self, what):
    return what.replace('\n', ' ')

  def collapse_whitespaces(self, what):
    return self.WHITESPACE_REGEX.sub(r' ', what)

  def split_non_word(self, what):
    return self.SPLIT_NONWORD_REGEX.split(what)

  def get_ext(self):
    return 'spelling.txt'

  def get_converted_row(self, idx, row):
    lines = []
    for value in row.values():
      value = self.remove_latex(value)
      value = self.remove_newlines(value)
      value = self.collapse_whitespaces(value)
      for split in self.split_non_word(value):
        split = split.strip('-')
        if len(split) < 2:
          continue
        if split.isdigit():
          continue
        lines.append(split)
    lines = set(lines)
    return '\n'.join(lines) + '\n'


def prepare_reading(sheet_files):
  print('prepare reading CSV files')

  csv_groups = {}

  for sheet_file in sheet_files:
    print(f'exporting all sheets in "{sheet_file}" to CSV')
    with TemporaryDirectory() as tmp_profile_dir_path:
      check_call((
        'localc',
        f'-env:UserInstallation=file://{tmp_profile_dir_path}',
        '--convert-to',

        # https://help.libreoffice.org/latest/en-US/text/shared/guide/csv_params.html
        'csv:Text - txt - csv (StarCalc):44,34,,,,,,,,,,-1',

        sheet_file
      ))

  for sheet_file in sheet_files:
    print(f'guess CSVs exported from "{sheet_file}"')
    base_path = splitext(sheet_file)[0]

    for csv_file in glob(base_path + '-*.csv'):

      print(f'guessed CSV file "{csv_file}"')
      sheet_name = csv_file[len(base_path)+1:-len('.csv')]

      print(f'guessed sheet name "{sheet_name}"')
      csv_groups.setdefault(sheet_name, []).append(csv_file)

  print(f'identified groups of CSV files: {csv_groups}')
  return csv_groups


def clean(row):

  def cleaned_items(row):
    for key, value in row.items():
      key = key.strip()
      assert key != '', f'column names must not be empty strings; ' \
        f'offending row: "{row}"'
      value = value.strip()
      if not value:
        continue
      yield key, value

  return dict(cleaned_items(row))


def read(csv_groups):
  print('reading CSV files')
  data_groups = {}

  for group, paths in csv_groups.items():
    print(f'reading CSV files for group {group}')
    data = data_groups.setdefault(group, {})

    for path in paths:
      print(f'reading CSV file {path}')

      with open(path) as csv_fp:
        for row in DictReader(csv_fp):
          row = clean(row)
          if not row:
            continue

          row_id = row.pop(ID_COLUMN_NAME)

          assert row_id not in data, \
            f'items in column "{ID_COLUMN_NAME}" must be unique ' \
            f'but "{row_id}" appears multiple times'

          data[row_id] = row

  return data_groups


def sort(data_groups):
  for group in data_groups:
    data_groups[group] = OrderedDict(sorted(data_groups[group].items()))
  return data_groups


def write(data_groups):

  print('collecting converters')
  converters = tuple(Cls() for Cls in CONVERTERS)

  for group, data in data_groups.items():
    print(f'converting group {group}')

    for converter in converters:
      print(f'converting with {converter}')

      converted = ''
      for idx, row in data.items():
        assert idx
        converted += converter.get_converted_row(idx, row)

      out_path = f'{group}.{converter.get_ext()}'
      print(f'writing to {out_path}')
      with open(out_path, 'w') as out_fp:
        out_fp.write(converter.get_preamble() or '')
        out_fp.write(converted)
        out_fp.write(converter.get_postamble() or '')


def main():
  ''' CLI and program coordination '''

  parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
  )

  parser.add_argument(
    'sheet', nargs='+', help=('input spreadsheet; entries are merged by'
    'tuple (sheet name in file, ID), later wins')
  )

  args = parser.parse_args()

  csv_groups = prepare_reading(args.sheet)
  data_groups = read(csv_groups)
  data_groups = sort(data_groups)
  write(data_groups)


if __name__ == '__main__':
  main()
