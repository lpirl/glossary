MAKEFLAGS+=-r

all: convert.log

%.log: %.py glossary.fods
	python3 $^ | tee $@

clean:
	git clean -Xi
